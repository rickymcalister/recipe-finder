<?php 
// Lazy load the required classes.
require_once('autoloader.php');

// Prompt the user to provide a list of ingredients ( CSV filename ).
echo "What's in your fridge? ";
fscanf(STDIN, "%s\n", $ingredientsCsv);

// Prompt the user to provide a set of recipes ( JSON filename ).
echo "What recipes would you like to choose from? ";
fscanf(STDIN, "%s\n", $recipesJson);

// Instantiate the RecipeFinder object and set the sample data filenames.
$recipeFinder = new RecipeFinder(
	array(
		'ingredientsCsv' => $ingredientsCsv,
		'recipesJson' => $recipesJson
	)
);

// Generate a list of ingredients from the supplied CSV file.
if (!$recipeFinder->getIngredients()) {
	echo "\nFailed to load ingredients!\n";
	exit;
}

// Generate a set of recipes from the supplied JSON file.
if (!$recipeFinder->getRecipes()) {
	echo "\nFailed to load recipes!\n";
	exit;
}

echo "\n" . $recipeFinder->recommendRecipe() . "\n";
?>
