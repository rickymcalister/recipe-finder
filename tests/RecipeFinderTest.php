<?php

class RecipeFinderTest extends \PHPUnit_Framework_TestCase {
	/**
     * @covers RecipeFinder::__construct
     */
    public function testObjectCanBeConstructedForValidConstructorArguments()
    {
        // Instantiate the RecipeFinder object with incorrect filenames.
		$recipeFinder = new RecipeFinder(
			array(
				'ingredientsCsv' => 'thisfiledoesnotexist.csv',
				'recipesJson' => 'thisfiledoesnotexist.json'
			)
		);

        $this->assertInstanceOf('recipeFinder', $recipeFinder);

        return $recipeFinder;
    }

	/**
	 * @uses  RecipeFinder::getIngredients
	 */
	public function testIncorrectCsvFilename() {
        // Instantiate the RecipeFinder object with incorrect filenames.
		$recipeFinder = new RecipeFinder(
			array(
				'ingredientsCsv' => 'thisfiledoesnotexist.csv',
				'recipesJson' => 'thisfiledoesnotexist.json'
			)
		);

        $this->assertFalse($recipeFinder->getIngredients());
    }

    /**
	 * @uses  RecipeFinder::getRecipes
	 */
	public function testIncorrectJsonFilename() {
        // Instantiate the RecipeFinder object with incorrect filenames.
		$recipeFinder = new RecipeFinder(
			array(
				'ingredientsCsv' => 'thisfiledoesnotexist.csv',
				'recipesJson' => 'thisfiledoesnotexist.json'
			)
		);

        $this->assertFalse($recipeFinder->getRecipes());
    }

    /**
	 * @uses  RecipeFinder::recommendRecipe
	 */
	public function testOrderTakeoutForOutOfDateIngredients() {
        // Instantiate the RecipeFinder object with a list of out of date recipes.
		$recipeFinder = new RecipeFinder(
			array(
				'ingredientsCsv' => 'tests/out_of_date_ingredients.csv',
				'recipesJson' => 'tests/recipes.json'
			)
		);

        $this->assertEquals('Order Takeout', $recipeFinder->recommendRecipe());
    }
}