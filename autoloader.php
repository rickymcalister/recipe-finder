<?php
	/**
	 * Use an anonymous function to resister the required classes for autoloading.
	 */
	spl_autoload_register(function ($class) {
	    require_once('recipeFinder/recipeFinder.php');
	});
?>