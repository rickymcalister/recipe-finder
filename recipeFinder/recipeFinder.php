<?php
/**
 * TODO: Add Exception Handler.
 */

class RecipeFinder {
	/**
	 * The ingredients CSV filename.
	 *
	 * @var str
	 */
	protected $ingredientsCsv;

	/**
	 * The ingredients.
	 *
	 * @var str
	 */
	protected $ingredients;

	/**
	 * The recipes JSON filename.
	 *
	 * @var str
	 */
	protected $recipesJson;

	/**
	 * The recipes.
	 *
	 * @var str
	 */
	protected $recipes;

	/**
	 * The recommended recipe.
	 *
	 * @var str
	 */
	protected $recommendedRecipe;

	/**
	 * Initialise a RecipeFinder application.
	 *
	 * The configuration:
	 * - ingredientsCsv : the CSV filename containing a set of ingredients.
	 * - recipesJson : the JSON filename containing a set of resipes.
	 *
	 * @param array $config The application configuration.
	 * @return void
	 */
	public function __construct($config) {
		if (isset($config['ingredientsCsv'])) {
			$this->setIngredientsCsv($config['ingredientsCsv']);
		}

		if (isset($config['recipesJson'])) {
			$this->setRecipesJson($config['recipesJson']);
		}
	}

	/**
	 * Set the CSV filename to be used for the ingredients list.
	 *
	 * @param str $filename The CSV filename to be used.
	 * @return object
	 */
	public function setIngredientsCsv($filename) {
		$this->ingredientsCsv = $filename;

		return $this;
	}

	/**
	 * Set the JSON filename to be used for the recipes list.
	 *
	 * @param str $filename The JSON filename to be used.
	 * @return object
	 */
	public function setRecipesJson($filename) {
		$this->recipesJson = $filename;

		return $this;
	}

	/**
	 * Load the ingredients from the CSV file and store the data as an array.
	 *
	 * @return object || bool
	 */
	public function getIngredients() {
		if (!is_null($this->ingredientsCsv) && file_exists($this->ingredientsCsv)) {
			$ingredients = array();

			// Loading CSV file and parsing the CSV contents.
			if (($handle = fopen($this->ingredientsCsv, "r")) !== FALSE) {
				while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				    $ingredients[] = $data;
				}
				fclose($handle);
			}

			$this->ingredients = $ingredients;
		} else {
			return false;
		}

		return $this;
	}

	/**
	 * Load the recipes from the JSON file and store the data as an array.
	 *
	 * @return object || bool
	 */
	public function getRecipes() {
		if (!is_null($this->recipesJson) && file_exists($this->recipesJson)) {
			$this->recipes = json_decode(file_get_contents($this->recipesJson), true);
		} else {
			return false;
		}

		return $this;
	}

	/**
	 * Get the recommended recipe.
	 *
	 * @return str The recommended recipe.
	 */
	public function recommendRecipe() {
		if (!$this->chooseRecipe()) {
			return "Order Takeout";
		}

		return $this->recommendedRecipe;
	}

	/**
	 * Choose the best fitting recipe based on the list of ingrients available.
	 *
	 * @return object
	 */
	public function chooseRecipe() {
		if (!empty($this->ingredients) && !empty($this->recipes)) {
			// Clear out any expired ingredients.
			$this->checkIngredientsUseBy();

			/**
			 * Generate a basic list of available ingredients. Ensure that the keys are maintained 
			 * as they can be used to help determine the use by date and therefore the recipe with 
			 * the nearest use by.
			 */
			$availableIngredients = array();
			foreach ($this->ingredients as $ingredientKey => $ingredient) {
				$availableIngredients[$ingredientKey] = $ingredient[0];
			}

			/**
			 * Iterate through each recipe and determine if all necessary ingredients are 
			 * available. 
			 */
			foreach ($this->recipes as $recipeKey => $recipe) {
				foreach ($recipe['ingredients'] as $recipeIngredientKey => $recipeIngredient) {
					// Determine the position of the current ingredient within the list of abailable ingredients.
					$ingredientPos = array_search($recipeIngredient['item'], $availableIngredients);

					if ($ingredientPos === false) {
						// The required ingredient is not available.
						unset($this->recipes[$recipeKey]);
						break;
					} else {
						/**
						 * Determine the use by date of the recipe by comparing the use by dates of the individual 
						 * ingredients. The nearest use by date should be set as the use by date of the recipe.
						 */
						if (!isset($this->recipes[$recipeKey]['useBy'])) {
							$this->recipes[$recipeKey]['useBy'] = $this->ingredients[$ingredientPos][3];
						} else {
							// Check if the use by date of the current ingredient is closer than the existing recipe use by date.
							$recipeUseBy = new DateTime(str_replace('/', '-', $this->recipes[$recipeKey]['useBy']));
							$ingredientUseBy = new DateTime(str_replace('/', '-', $this->ingredients[$ingredientPos][3]));

							// Use the nearest use by date to determine the use by date of the recipe.
							$this->recipes[$recipeKey]['useBy'] = $recipeUseBy < $ingredientUseBy ? $this->recipes[$recipeKey]['useBy'] : $this->ingredients[$ingredientPos][3]; 
						}
					}
				}
			}

			/**
			 * If there is more than one recipe to choose from, determine which recipe to select based on the fact 
			 * that the recipe with the nearest use by date should be recommended first. This can be achieved by 
			 * sorting the available recipes based on the use by date and selecting the first item in the list.
			 */
			if (!empty($this->recipes)) {
				if (count($this->recipes) > 1) {
					// Initiate a user defined sort to order the recipes by the use by date.
					usort($this->recipes, array($this,'compareDates'));
				}

				$this->recommendedRecipe = $this->recipes[0]['name'];
			} else {
				return false;
			}
		} else {
			return false;
		}

		return $this;
	}

	/**
	 * Check the use by date of the ingredients and remove any that have expired.
	 *
	 * @return object || bool
	 */
	protected function checkIngredientsUseBy() {
		if (!is_null($this->ingredients)) {
			/**
			 * Iterate through the list of ingredients and remove any that are past the 
			 * use by date.
			 */
			foreach ($this->ingredients as $ingredientKey => $ingredient) {
				$today = new DateTime();

				// Format the use by date as a valid date string that can be interpreted as a DateTime object.
				$useBy = new DateTime(str_replace('/', '-', $ingredient[3]));

				// If today's date is greater than the use by date, then remove the ingredient from the available options.
				if ($today > $useBy) {
					unset($this->ingredients[$ingredientKey]);
				}
			}
		} else {
			return false;
		}

		return $this;
	}

	/**
	 * Date comparison function.
	 *
	 * @param array $a
	 * @param array $b
	 * @return int
	 */
	protected function compareDates($a, $b) {
		// Create date objects for comparison
		$dateA = new DateTime(str_replace('/', '-', $a['useBy']));
	    $dateB = new DateTime(str_replace('/', '-', $b['useBy']));

	    if ($dateA == $dateB) {
	        return 0;
	    }

	    return ($dateA > $dateB) ? 1 : -1;
	}
}